import React, { useRef } from 'react';
import Sketch from 'react-p5';
import MersenneTwister from 'mersenne-twister';
import reactDom from 'react-dom';

/*
Create your Custom style to be turned into a EthBlock.art Mother NFT

Basic rules:
 - use a minimum of 1 and a maximum of 4 "modifiers", modifiers are values between 0 and 1,
 - use a minimum of 1 and a maximum of 3 colors, the color "background" will be set at the canvas root
 - Use the block as source of entropy, no Math.random() allowed!
 - You can use a "shuffle bag" using data from the block as seed, a MersenneTwister library is provided

 Arguments:
  - block: the blockData, in this example template you are given 3 different blocks to experiment with variations, check App.js to learn more
  - mod[1-3]: template modifier arguments with arbitrary defaults to get your started
  - color: template color argument with arbitrary default to get you started

Getting started:
 - Write p5.js code, comsuming the block data and modifier arguments,
   make it cool and use no random() internally, component must be pure, output deterministic
 - Customize the list of arguments as you wish, given the rules listed below
 - Provide a set of initial /default values for the implemented arguments, your preset.
 - Think about easter eggs / rare attributes, display something different every 100 blocks? display something unique with 1% chance?

 - check out p5.js documentation for examples!
*/

let DEFAULT_SIZE = 500;
const CustomStyle = ({
    block,
    canvasRef,
    attributesRef,
    width,
    height,
    handleResize,
    mod1 = 0.07, // Example: replace any number in the code with mod1, mod2, or color values
    mod2 = 0.05,
    background = '#000000'
}) => {
    const shuffleBag = useRef();

    const { hash } = block;
    let img = useRef();
    let iconString = useRef();
    let iconsEsp = useRef();
    let iconsCrypto = useRef();
    let blockNumberOld = useRef();
    let cryptoMode = useRef();
    let colsNum = useRef();
    let rowsNum = useRef();
    let col1 = useRef();
    let col2 = useRef();
    let colBg = useRef();

    // setup() initializes p5 and the canvas element, can be mostly ignored in our case (check draw())
    const setup = (p5, canvasParentRef) => {
        // Keep reference of canvas element for snapshots
        let _p5 = p5.createCanvas(width, height).parent(canvasParentRef);
        canvasRef.current = p5;

        blockNumberOld.current = -1;
        cryptoMode.current = false;

        attributesRef.current = () => {
            return {
                // This is called when the final image is generated, when creator opens the Mint NFT modal.
                // should return an object structured following opensea/enjin metadata spec for attributes/properties
                // https://docs.opensea.io/docs/metadata-standards
                // https://github.com/ethereum/EIPs/blob/master/EIPS/eip-1155.md#erc-1155-metadata-uri-json-schema

                attributes: [
                    {
                        trait_type: 'Icon',
                        value: iconString.current,
                    },
                    {
                        display_type: 'number',
                        trait_type: 'Columns',
                        value: colsNum.current,
                    },
                    {
                        display_type: 'number',
                        trait_type: 'Rows',
                        value: rowsNum.current,
                    },
                    {
                        trait_type: 'Crypto Mode',
                        value: cryptoMode.current.toString(),
                    },
                    {
                        trait_type: 'Color 1',
                        value: col1.current.toString(),
                    },
                    {
                        trait_type: 'Color 2',
                        value: col2.current.toString(),
                    },
                    {
                        trait_type: 'Background Color',
                        value: colBg.current.toString(),
                    },
                    
                ],
            };
        };
    };

    const preload = (p5) => {
        // preload all images and store in array
        const iconsEspStrings = ['icon-square.png', 'icon-circle.png', 'icon-plus.png', 'icon-star.png', 'icon-waves.png'];
        const iconsCryptoStrings = ['icon-nft.png'];
        iconsEsp.current = iconsEspStrings.map((imgString) => p5.loadImage(imgString));
        iconsCrypto.current = iconsCryptoStrings.map((imgString) => p5.loadImage(imgString));
    };

    const getImg = (p5) => {
        // check cryptoMode
        const seed = parseInt(hash.slice(0, 16), 16);
        shuffleBag.current = new MersenneTwister(seed);
        const shuffle1 = shuffleBag.current.random();

        cryptoMode.current = false;
        if (shuffle1 < 0.025) {
            cryptoMode.current = true;
        }

        // what image should be used
        let myImg, imgID, iconStrings;
        if (cryptoMode.current) {
            imgID = Math.round(p5.map(sumDigits(block.number), 1, 9, 0, iconsCrypto.current.length - 1));
            myImg = iconsCrypto.current[imgID];

            iconStrings = ['NFT'];
            iconString.current = iconStrings[imgID];
        } else {
            imgID = Math.round(p5.map(sumDigits(block.number), 1, 9, 0, iconsEsp.current.length - 1));
            myImg = iconsEsp.current[imgID];

            iconStrings = ['Square', 'Circle', 'Plus', 'Star', 'Waves'];
            iconString.current = iconStrings[imgID];
        }
        return myImg;
    }
    const sumDigits = (num) => {
        let sum = 0;
        let val = num;

        while (val) {
            sum += val % 10;
            val = Math.floor(val / 10);
        }

        if (sum < 10) {
            return sum;
        } else {
            return sumDigits(sum);
        }
    }


    const draw = (p5) => {
        let WIDTH = width;
        let HEIGHT = height;
        let DIM = Math.min(WIDTH, HEIGHT);
        let M = DIM / DEFAULT_SIZE;
        p5.clear();
        

        // colors
        let col1Hex = '#' + block.gasLimit.hex.substring(2, 8);
        col1.current = p5.color(col1Hex);
        // let col2Hex = '#' + block.gasUsed.hex.substring(2, 8);
        // col2.current = p5.color(col2Hex);
        let r2 = 255 - p5.red(col1.current);
        let g2 = 255 - p5.green(col1.current);
        let b2 = 255 - p5.blue(col1.current);
        col2.current = p5.color(r2, g2, b2);

        // bg color
        if (cryptoMode.current) {
            p5.blendMode(p5.BLEND);
            colBg.current = p5.color(255, 255, 255);
        } else {
            p5.blendMode(p5.ADD);
            colBg.current = p5.color(background);
        }
        p5.background(colBg.current);


        // change image if block has changed
        if (block.number !== blockNumberOld.current) {
            img.current = getImg(p5);
        }


        // setup grid
        let maxW = DEFAULT_SIZE;
        let colW = p5.map(mod1, 0, 1, 5, DEFAULT_SIZE / 2);
        let halfRestW = (maxW - colW) / 2;
        let extraColsHalf = Math.floor(halfRestW / colW);
        let colOffsetX = halfRestW - (extraColsHalf * colW);
        let cols = 1 + (extraColsHalf * 2);
        colsNum.current = cols;

        

        let maxH = DEFAULT_SIZE;
        let rowH = p5.map(mod2, 0, 1, 5, DEFAULT_SIZE / 2);
        let halfRestH = (maxH - rowH) / 2;
        let extraRowsHalf = Math.floor(halfRestH / rowH);
        let rowOffsetY = halfRestH - (extraRowsHalf * rowH);
        let rows = 1 + (extraRowsHalf * 2);
        rowsNum.current = rows;

        let modAvg = (mod1 + mod2) / 2;
        let alpha = 75;
        let cutoff = 0.2;
        if (modAvg > cutoff){
            alpha = p5.map(modAvg, cutoff, 1, 75, 220);
        }


        // create multidimensional array to store brightness values
        img.current.loadPixels();
        let arrBright = [];
        let pixels = img.current.pixels;
        for (let y = 0; y <= rows; y++) {
            let arrRow = []
            for (let x = 0; x <= cols; x++) {
                let xPos = Math.floor(colOffsetX + (x * colW));
                let yPos = Math.floor(rowOffsetY + (y * rowH));
                let i = (xPos + yPos * img.current.width) * 4;
                let p = pixels[i];
                let b = p5.brightness(p);
                arrRow.push(b);
            }
            arrBright.push(arrRow);
        }


        // draw vertical shapes
        p5.noFill();
        p5.noStroke();
        if (cryptoMode.current) {
            col1.current = p5.color(0, 0, 0);
            p5.stroke(col1.current);
        } else {
            let c = col1.current;
            c.setAlpha(alpha);
            p5.fill(c);
        }
        let maxHShape = DEFAULT_SIZE / rows * 1 * M;
        for (let x = 0; x <= cols; x++) {
            let arrH = [];
            for (let y = 0; y <= rows; y++) {
                let b = arrBright[y][x];
                let h = p5.map(b, 0, 100, 0, maxHShape);
                arrH.push(h);
            }

            p5.beginShape();
            // left to right
            for (let y = 0; y < arrH.length; y++) {
                let h = arrH[y];
                let xPos = (((x * colW) + colOffsetX) - h) * M;
                let yPos = (((y * rowH) + rowOffsetY)) * M;
                p5.curveVertex(xPos, yPos);
            }
            // right to left
            for (let y = arrH.length - 1; y >= 0; y--) {
                let h = arrH[y];
                let xPos = (((x * colW) + colOffsetX) + h) * M;
                let yPos = (((y * rowH) + rowOffsetY)) * M;
                p5.curveVertex(xPos, yPos);
            }
            p5.endShape(p5.CLOSE);
        }


        // draw horizontal shapes
        p5.noFill();
        p5.noStroke();
        if (cryptoMode.current) {
            col2.current = p5.color(0, 0, 0);
            p5.stroke(col2.current);
        } else {
            let c = col2.current;
            c.setAlpha(alpha);
            p5.fill(c);
        }
        let maxWShape = DEFAULT_SIZE / cols * 1 * M;
        for (let y = 0; y <= rows; y++) {
            let arrH = [];
            for (let x = 0; x <= cols; x++) {
                let b = arrBright[y][x];
                let w = p5.map(b, 0, 100, 0, maxWShape);
                arrH.push(w);
            }
            p5.beginShape();
            // top to bottom
            for (let x = 0; x < arrH.length; x++) {
                let h = arrH[x];
                let xPos = ((x * colW) + colOffsetX) * M;
                let yPos = (((y * rowH) + rowOffsetY) - h) * M;
                p5.curveVertex(xPos, yPos);
            }
            // bottom to top
            for (let x = arrH.length - 1; x >= 0; x--) {
                let h = arrH[x];
                let xPos = ((x * colW) + colOffsetX) * M;
                let yPos = (((y * rowH) + rowOffsetY) + h) * M;
                p5.curveVertex(xPos, yPos);
            }
            p5.endShape(p5.CLOSE);
        }

        blockNumberOld.current = block.number
    };

    return <Sketch preload={preload} setup={setup} draw={draw} windowResized={handleResize} />;
};

export default CustomStyle;

const styleMetadata = {
    name: 'Cryptoid Alchemy',
    description: 'Morph the shapes of the archetypal ESP signs or glitch out and create an abstract composition. Or use your extrasensory perception to bend reality and find those monochrome black magic crypto gems. Shapes and hidden gems are chosen based on a combination of block hash and block number, colors are selected based on gas limit.',
    image: '',
    creator_name: 'Moodsoup',
    options: {
        mod1: 0.07,
        mod2: 0.05,
        background: '#000000'
    },
};

export { styleMetadata };
